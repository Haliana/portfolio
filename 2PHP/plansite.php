<!DOCTYPE html>

<html lang="fr">

	<!-- En-tête de la page -->

    <head>
		<link rel="shortcut icon" href="../image/Général/code-interface.jpg" type="image/x-icon"/>
        <title>Plan de Site</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
    </head>

	<!-- Corps de la page -->

    <body>
		<header id="header">
			<a href="../index.php">
				<div class ="border1">
					<h2>Plan de Site</h2>
				</div>
			</a>
		</header>
		
		<main>
			<div id="plan">
				- <a href="../index.php">Accueil</a><br>
				- <a href="presentation.php">Présentation</a><br>
				- <a href="btssio.php">BTS SIO</a><br>
				- <a href="competences.php">Compétences :</a><br>
					- <a href="#"></a><br>
					- <a href="#"></a><br>
				- <a href="realisations.php">Réalisations</a><br>
				- <a href="veille.php">Veille Technologique</a><br>
			</div>
		</main>
	</body>
</html>
