<!DOCTYPE html>

<html lang="fr">

	<!-- En-tête de la page -->

    <head>
		<link rel="shortcut icon" href="../image/Général/code-interface.jpg" type="image/x-icon"/>
        <title>Compétences</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
    </head>

	<!-- Corps de la page -->

    <body>
		<header>
			<a href="../index.php">
				<h1>Compétences</h1>
			</a>
		</header>
		
		<?php include('menu.php'); ?>

		<main>
			<h2>Programmation</h2>
				<ul>
					<li>Langage C</li>
					<li>Java</li>
					<li>HTML</li>
					<li>CSS</li>
				</ul>
			
			<h2>Réseau</h2>
				<ul>
					<li>Cicso Packet Tracer</li>
					<li>Création d'un réseau LAN</li>
				</ul>
			
			<h2>Pack Office</h2>
				<ul>
					<li>Excel</li>
					<li>Publisher</li>
					<li>Word</li>
				</ul>
			
			<h2>Anglais</h2>
				<ul>
					<li>Compréhension et expression écrite</li>
					<li>Compréhension et expression orale</li>
				</ul>
		</main>

		<?php include('footer.php'); ?>
	
	</body>
</html>