<!DOCTYPE html>

<html lang="fr">

	<!-- En-tête de la page -->

    <head>
		<link rel="shortcut icon" href="../image/code-interface-symbol-of-signs_318-53866.jpg" type="image/x-icon"/>
        <title>Réalisations</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
    </head>

	<!-- Corps de la page -->

    <body>
		<header>
			<a href="../index.php">
				<h1>Réalisations</h1>
			</a>
		</header>
		
		<?php include('menu.php'); ?>

		<main>
			<div class="h1">En Centre de Formation</div>
			<p></p>

			<div class="h1">En Entreprise</div>
			<p></p>

			<div class="h1">Personnelles</div>
			<p></p>
		</main>
		
		<?php include('footer.php'); ?>
	</body>
</html>