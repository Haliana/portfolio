<!DOCTYPE html>

<html lang="fr">
    <head>
		<link rel="shortcut icon" href="../image/Général/code-interface.jpg" type="image/x-icon"/>
		<title>BTS SIO</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
		<script src="to-top.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    </head>

    <body>
    	<?php include('header.php'); ?>

		<?php include('menu.php'); ?>

		<main>
			<div id="btssio1">
				<h1>Le BTS</h1>
				<p>
					Le BTS SIO (<span class="italic">Services Informatiques aux Organisation</span>)

					option SLAM (<span class="italic">Solutions Logicielles et Applications Métiers</span>)
				</p>
			</div>
			
			<div id="matieres">
				<h1>Les Matières</h1>
				<ul>
					<li>Enseignements généraux
						<ul>
							<li>Maths</li>
							<li>Anglais</li>
							<li>Culture Générale et Expression</li>
							<li>Economie, Management et Droit</li>
						</ul>
					</li>

					<li>Enseignements professionnels
						<ul>
							<li>Réseau</li>
							<li>Algorithmique</li>
							<li>Option SLAM</li>
							<li>Option SISR</li>
						</ul>
					</li>
				</ul>
			</div>
			
			<div id="epreuves">
				<h1>Les Epreuves</h1>
				<ul>
					<li><span class="epreuves">Enseignements généraux</span>
						<ul>
							<li><span class="epreuves">E1</span> : <span class="nom_epreuves">Culture et Communication</span>
								<ul>
									<li><span class="epreuves">U11</span> - <span class="nom_epreuves">Culture Générale et Expression</span><br>
										Epreuve écrite<br>
										Coef 2<br>
										Durée 4H
									</li>
									<li><span class="epreuves">U12</span> - <span class="nom_epreuves">Anglais</span><br>
										Epreuve écrite et orale<br>
										Coef 2<br>
										Durée 2H à l'écrit et 20 min à l'oral (20 min de préparation)
									</li>
								</ul>
							</li>

							<li><span class="epreuves">E2</span> : <span class="nom_epreuves">Mathématiques pour l'Informatique</span>
								<ul>
									<li><span class="epreuves">U21</span> - <span class="nom_epreuves">Mathématiques</span><br>
										Epreuve écrite<br>
										Coef 3<br>
										Durée 3H
									</li>
									<li><span class="epreuves">U22</span> - <span class="nom_epreuves">Algorithmie Appliquée</span><br>
										Epreuve Contrôle en Cours de Formation et orale<br>
										Coef 1<br>
										Durée 20 min (1H de préparation)
									</li>
								</ul>
							</li>

							<li><span class="epreuves">E3</span> : <span class="nom_epreuves">Analyse Economique, Managériale et Juridique des Services Informatiques</span><br>
								Epreuve Ecrite<br>
								Coef 4<br>
								Durée 4H
							</li>
						</ul>
					</li>
			
					<li><span class="epreuves">Enseignements professionnels</span>
						<ul>
							<li><span class="epreuves">E4</span> : <span class="nom_epreuves">Conception et Maintenance de Solutions Informatiques</span><br>
								Epreuve Contrôle en Cours de Formation orale et pratique<br>
								Coef 4<br>
								Durée 20 min (30 min de préparation) à l'oral + 20 min (1H de préparation) en pratique
							</li>

							<li><span class="epreuves">E5</span> : <span class="nom_epreuves">Production et Fourniture de Services Informatiques</span><br>
								Epreuve écrite<br>
								Coef 4 <br>
								Durée 5H
							</li>

							<li><span class="epreuves">E6</span> : <span class="nom_epreuves">Parcours de Professionalisation</span><br>
								Epreuve orale<br>
								Coef 3
								Durée 40 min
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div id="perspectives">
				<h1>Les Perspectives d'Emplois</h1>
				<ul>
					<li>Entreprises</li>
					<li>Administrations</li>
					<li>Sociétés de services en informatique</li>
					<li>Sociétés éditrice de logiciel</li>
				</ul>
			</div>
		</main>
	</body>

	<?php include('footer.php'); ?>
</html>