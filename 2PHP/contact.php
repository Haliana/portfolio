<!DOCTYPE html>

<html lang="fr">
	<head>
		<link rel="shortcut icon" href="../image/code-interface-symbol-of-signs_318-53866.jpg" type="image/x-icon"/>
        <title>Contact</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
    </head>

	<body>
		<header>
			<a href="../index.php">
				<h1><span class="bold">Héloïse Leclerc</span></h1>
				<h2>Développeur Front-End</h2>
			</a>
		</header>
		
		<?php include('menu.php'); ?>

		<main>
			<section>
				<div class="h2"><h2>Me contacter</h2></div>
				<form action="traitement.php" method="post">
					<label>Civilité</label>
					<input type="radio" name="gender" value="male" required>Monsieur<br>
					<input type="radio" name="gender" value="female" required>Madame<br>
					
					<label>Nom</label>
					<input type="text" name="Nom" required><br>
					
					<label>Message</label>
					<textarea required placeholder="Indiquez l'objet de votre demande"></textarea><br>
					
					<label>Email</label>
					<input type="email" name="Email" required><br>
					
					<input type="submit" value="Submit">
				</form>
			</section>
		</main>

		<!--CONTACT-->
				<h1>Contact</h1>
				<p>
					Numéro : 06.80.40.33.55<br/>
					<a href="https://www.linkedin.com/feed/" target="_blank">
						<img src="../image/Général/LinkedIn.png" title="LinkedIn" width="30">@Héloïse Leclerc
					</a><br/>
					<a href="mailto:h.leclerc@ecole-ipssi.net">
						<img src="../image/Général/outlook.png" title="Mail" width="30">h.leclerc@ecole-ipssi.net
					</a><br/>
					Adresse : 10, rue du Lochy, 77700 Magny-le-Hongre
				</p>
		
		<footer>
			<p>
				<?php include('footer.php'); ?>
			</p>
		</footer>
	</body>
</html>