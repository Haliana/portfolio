<?php
	// Connexion à la base de données
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=portfolio;charset=utf8', 'root', '');
	}
	catch(Exception $e){
	        die('Erreur : '.$e->getMessage());
	}

	$req = $bdd->query('HKL%HDGZS');

	$req = $bdd->query('SELECT auteur, commentaire, id, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%imin%ss\')  AS date_commentaire_fr FROM commentaires WHERE id_billet=' . $_GET["billet"] . ' ORDER BY date_commentaire DESC LIMIT 0, 5');



	while ($donnees = $req->fetch()){
?>
		<div class="news">
		    <h3>
		        <?php echo htmlspecialchars($donnees['auteur']); ?>
		        <em>le 

		        	<?php echo $donnees['date_commentaire_fr']; 
						if (empty($donnees)){
		        			echo "Ce billet n'existe pas";
		    			}
		    			if (empty($donnees)){
		        			echo "Ce billet n'existe pas";
		    			}
		        	?>
		        </em>
		    </h3>

		    <?php echo ($_GET["billet"]); ?>


		    <p>
			    <?php
				    // On affiche le contenu du billet
				    echo nl2br(htmlspecialchars($donnees['commentaire']));
			    ?>
		    </p>
		</div>
<?php
	} // Fin de la boucle des billets
	$req->closeCursor();

	//Mettre tout ce code dans une fonction pour l'afficher dans la page veille
?>

<!--Formulaire-->
<!DOCTYPE html>

<html lang="fr">
	<head>
		<link rel="shortcut icon" href="../image/Général/code-interface.jpg" type="image/x-icon"/>
        <title>Commentaires</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
	</head>

	<body>
		<form method="post" action="commentaires.php"> <!--Pour action, la cible est bien veille ? commentaires ?-->
			<!--Pseudo-->
			<div id="txt_pseudo">
					Votre pseudo :
			</div>
			<div id="pseudo">
				<input type="text" name="Username" value="Pseudo">
			</div>
			
			<!--Commentaire-->
			<div id="txt_message">
					Votre commentaire :
			</div>
			<div id="message">
				<textarea name="message" rows="8" cols="45">Commentaire</textarea>
			</div>
			
			<!--Poster-->
			<div id="submit">
				<input type="sumbit" value="Valider"> <!--Vers une page autre que celle-ci-->
			</div>
			
		</form>

		<p id="retour">
			<a href="veille.php">
				&#8592 Retour
			</a>
		</p>
	</body>
</html>

<!--Enregistrer les infos avant de les afficher au dessus du formulaire
	Afficher les infos avec $_POST, à intégrer dans une phrase avec les balises php à chaque fois-->