<!DOCTYPE html>

<html lang="fr">
    <head>
		<link rel="shortcut icon" href="../image/Général/code-interface.jpg" type="image/x-icon"/>
        <title>Présentation</title>
        <meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../portfolio.css">
    </head>

    <body>
		<header>
			<h1>Présentation</h1>
			<a href="../index.php">
				<img id="accueil" src="../image/Général/code-interface.jpg">
			</a>
		</header>

		<!--MENU-->
		<?php include('menu.php'); ?>

		<!--CONTENUS-->
		<main id="to-top">
			<div id="pond">
				<div class="polaroid">
					<a href="https://cvdesignr.com/p/5d2884bc82839?hl=fr_FR" target="_blank">
						<img class="cv_icon" src="../image/Général/logo_cvdesignr.jpg" title="CV Héloïse LECLERC">
						<div class="legend-container">
							<p class="legend">
								Consultez mon CV en ligne
							</p>
						</div>
					</a>
				</div>

				<div class="polaroid">
					<a href="../Médias/CV_Heloise_LECLERC.pdf" target="_blank" download>
						<img class="cv_icon" src="../image/Général/icon_pdf.png" title="CV Héloïse LECLERC">
						<div class="legend-container">
							<p class="legend">
								Télechargez mon CV (PDF)
							</p>
						</div>
					</a>
				</div>
			</div>
			
			<h2>Présentation</h2>
			<a href="../index.php"><img class="displayed" src="../photo.jpg" alt=”photo”></a><br>
			<!--PRESENTATION-->
			<p id="p1">
				Je suis étudiante de 21 ans en BTS SIO (<span class="italic" style="font-size: 0.9em;">Services Informatiques aux Organisation</span>)<br/>option SLAM (<span class="italic" style="font-size: 0.9em;">Solutions Logicielles et Applications Métiers</span>).<br/>
				<br/>Dans le cadre de ma formation, je suis actuellement à la recherche d'une alternance<br/>pour un poste de <span class="bold" style="color: #7303C0;">développeur front-end</span> d'une durée d'1 an sur l'année 2019-2020.<br>
				<br>A l'issue de l'obtention de mon BTS, je souhaite poursuivre dans le Web<br> en tant que développeur Front-End. Je suis toute fois ouverte à de nouveaux projets d'avenir.
			</p>

			<!--EXPERIENCES--> <!--Utiliser grid pour les placer ?-->
			<h2>Expérience</h2>

			<div class="polaroid2">
				<a class="center2" href="https://www.leclercdrive.fr/" target="_blank">
					<img class="cv_icon" src="../image/Général/leclerc-drive.jpg" title="E.Leclerc Drive" alt="logo de E.Leclerc Drive" width="50">
					<div class="mask2">
						<p>
							<ul class="details">
								<li>Préparation de commandes</li>
								<li>Livraison des commandes aux clients</li>
							</ul>
						</p>
					</div>
				</a>
				<span class="legend2">
					<h3>Préparatrice de commandes</h3>
					<span class="italic">E.Leclerc Drive, Torcy
					<br>Fin Janvier 2019 à fin août 2019</span>
				</span>
			</div>
			
			<div class="polaroid">
				<a class="center2" href="http://www.alphabusiness.fr/" target="_blank">
					<img class="cv_icon" src="../image/Général/alphabusiness.png" title="Alpha Business Solutions d'Entreprise" alt="Logo d'Alpha Business" width="50">
					<div class="mask2">
						<p>
							<ul class="details">
								<li>Création et ajustement d'une newsletter</li>
								<li>Création d'un site WordPress via l'hébergeur Plesk</li>
								<li>Création d'un site en HTML5/CSS3</li>
							</ul>
						</p>
					</div>
				</a>
				<p class="legend2">
					<h3>Stagiaire Développeuse Web</h3>
					<span class="italic">Alpha Business, Bussy-Saint-Georges
					<br/>Mars et Avril 2019</span>
				</p>
			</div>

			<div class="polaroid">
				<a class="center2" href="https://www.explorershotels.com/" target="_blank">
					<img class="cv_icon" src="../image/Général/explorers.png" title="Hôtel Explorers" alt="Logo de l'hôtel Explorers" width="50">
					<div class="mask2">
						<p>
							<ul class="details">
								<li>Service en salle</li>
								<li>Orienter les clients</li>
								<li>Plonge</li>
								<li>Encaissement des clients</li>
								<li>Fermeture de boutique</li>
							</ul>
						</p>
					</div>
				</a>
				<p class="legend2">
					<h3>Extra commis de salle</h3>
					<span class="italic">Hôtel Explorers, Magny-le-Hongre
						<br>Septembre à Octobre 2018
						<br>Septembre à Novembre 2019
					</span>
				</p>
			</div>

			<div class="polaroid">
				<a class="center2" href="https://www.societegenerale.fr/" target="_blank">
					<img class="cv_icon" src="../image/Général/sg.png" title="Société Générale" alt="Logo de la Société Générale" width="50">
					<div class="mask2">
						<p>
							<ul class="details">
								<li>Gérer les appels</li>
								<li>Trier les archives</li>
								<li>Orienter les clients</li>
							</ul>
						</p>
					</div>
				</a>
				<p class="legend2">
					<h3>Auxiliaire de vacances</h3>
					<span class="italic">Agence de la Société Générale, Esbly
					<br>Août 2018</span>
				</p>
			</div>

			<div class="polaroid">
				<a class="center2" href="https://www.aphp.fr/contenu/hopital-armand-trousseau-1" target="_blank">
					<img class="cv_icon" src="../image/Général/armand-trousseau.jpg" title="Hôpital AP-HP Armand Trousseau" alt="Logo de l'Hôpital " width="50">
					<div class="mask2">
						<p>
							<ul class="details">
								<li>Observation des manipulations</li>
								<li>Observation de caryotypes</li>
							</ul>
						</p>
					</div>
				</a>
				<p class="legend2">
					<h3>Stagiaire en laboratoire cytologique</h3>
					<span class="italic">Hôpital AP-HP Armand-Trousseau, Paris 12ème
					<br/>Avril 2017</span>
				</p>
			</div>

			<!-- FORMATION -->
		</main>

		<?php include('footer.php'); ?>

		<a id="cRetour" class="cInvisible" href="#to-top" style="display: inline;">
			<span id="toTopHover"></span>
			<img src="../image/Général/toTop.png" alt="To Top" width="40" height="40">
		</a>
	</body>
</html>


<!--
Expériences :
Mettre les XP autre que l'info à part (ajouter un bouton *voir mes autres expériences* en bas ?)
Faire que des tuiles (images) avec le nom de l'XP etc (voir le portfolio de Joss en exemple)
-->