<nav>
	<ul>
		<!--INDEX-->
		<li class="index">
			<a href="../index.php">
				Accueil
			</a>
		</li>

		<!--PRESENTATION-->
		<li class="presentation">
			<a href="presentation.php">
				Présentation
			</a>
		</li>

		<!--BTS SIO-->
		<li class="btssio">
			<a href="btssio.php">
				BTS SIO
			</a>
		</li>

		<!--COMPETENCES-->
		<li class="competences">
			<a href="competences.php">
				Compétences
			</a>
			<ul class="submenu">
				<li>
					<a href="#">
						En Centre de Formation
					</a>
				</li>
				<li>
					<a href="#">
						En Entreprise
					</a>
				</li>
				<li>
					<a href="#">
						Personnelles
					</a>
				</li>
			</ul>
		</li>

		<!--REALISATIONS-->
		<li class="realisations">
			<a href="realisations.php">
				Réalisations
			</a>
			<ul class="submenu">
				<li>
					<a href="#">
						En Centre de Formation
					</a>
				</li>
				<li>
					<a href="#">
						En Entreprise
					</a>
				</li>
				<li>
					<a href="#">
						Personnelles
					</a>
				</li>
			</ul>
		</li>

		<!--VEILLE TECHNOLOGIQUE-->
		<li class="veille">
			<a href="veille.php">
				Veille Technologique
			</a>
		</li>
	</ul>
</nav>