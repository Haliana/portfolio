<!DOCTYPE html>

<html lang="fr">
    <head>
		<link rel="shortcut icon" href="image/Général/code-interface.jpg" type="image/x-icon"/>
        <title>Héloïse Leclerc - Développeur Front-End</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="portfolio.css">
		<script src="to-top.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    </head>

    <body>
		<header>
			<h2>Bienvenue sur le portfolio de<br/></h2>
			<h1>Héloïse Leclerc</h1>
		</header>

		<?php include('2PHP/step-down.php'); ?>

		<main id="step-down">
			<!--CONTENU-->
			<div class="grid-container" id="to-top">
				<!--PRESENTATION-->
				<div id="presentation" class="item">
					<a class="center" href="2PHP/presentation.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/presentation.jpg" alt="Présentation"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								PRESENTATION
							</p>
							<ul class="img_list">
								<li>Parcours</li>
								<li>Formations</li>
								<li>Persectives d'avenir</li>
							</ul>
						</div>
					</a>
				</div>

				<!--BTS SIO-->
				<div id="btssio" class="item">
					<a class="center" href="2PHP/btssio.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/btssio.jpg" alt="BTS SIO"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								BTS SIO
							</p>
							<ul class="img_list">
								<li>Présentation du BTS</li>
								<li>Les matières</li>
								<li>Les épreuves</li>
								<li>Persectives d'emplois grâce au BTS</li>
							</ul>
						</div>
					</a>
				</div>

				<!--COMPETENCES-->
				<div id="item3" class="item">
					<a class="center" href="2PHP/competences.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/competences.jpg" alt="Compétences"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								COMPETENCES
							</p>
							<ul class="img_list">
								<li><span class="underline">En centre de formation</span>
									<ul>
										<li>Pendant les PPE</li>
										<li>Pendant les TP</li>
									</ul>
								</li>
								<li><span class="underline">En enterprise</span>
									<ul>
										<li>En stage</li>
										<li>En alternance</li>
									</ul>
								</li>
							</ul>
						</div>
					</a>
				</div>

				<!--REALISATIONS-->
				<div id="realisations" class="item">
					<a class="center" href="2PHP/realisations.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/realisations.jpg" alt="Réalisations"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								REALISATIONS
							</p>
							<ul class="img_list">
								<li><span class="underline">En centre de formation</span>
									<ul>
										<li>Pendant les PPE</li>
										<li>Pendant les TP</li>
									</ul>
								</li>
								<li><span class="underline">En enteprise</span>
									<ul>
										<li>En stage</li>
										<li>En alternance</li>
									</ul>
								</li>
								<li><span class="underline">Perso</span></li>
							</ul>
						</div>
					</a>
				</div>

				<!--VEILLE-->
				<div id="veille" class="item">
					<a class="center" href="2PHP/veille.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/veilletechno.jpg" alt="Veille Technologique"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								VEILLE TECHNOLOGIQUE
							</p>
						</div>
					</a>
				</div>
				<!--Ajouter un flux RRS ?-->
			</div>

			<!--CONTACT-->
				<div id="contact" class="item">
					<a class="center" href="2PHP/contact.php"><!--lien de la page-->
						<img class="logo" src="image/categories-index/veilletechno.jpg" alt="Veille Technologique"/><!--lien de l'image-->
						<div class="mask">
							<p class="img_description">
								CONTAC
T							</p>
						</div>
					</a>
				</div>
				<!--Ajouter un flux RRS ?-->
			</div>			
		</main>

		<?php include('2PHP/footer.php'); ?>
		
		<?php include('2PHP/to-top.php'); ?>
	</body>
</html>